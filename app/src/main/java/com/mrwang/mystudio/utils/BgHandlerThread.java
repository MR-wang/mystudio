package com.mrwang.mystudio.utils;

import android.os.*;
import android.os.Process;

/**
 * handlerThread类 提供一个子线程的轮询机制
 */
public class BgHandlerThread {

    public BgHandlerThread() {
        String TAG="BackgroundThread";
        HandlerThread handlerThread = new HandlerThread(TAG, Process.THREAD_PRIORITY_BACKGROUND);
        BgHandler handler=new BgHandler(handlerThread.getLooper());
    }
}
