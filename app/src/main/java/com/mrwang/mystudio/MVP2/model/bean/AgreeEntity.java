package com.mrwang.mystudio.MVP2.model.bean;

import java.util.List;

/**
 * 约课返回的javabean
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-12-21
 * Time: 22:03
 */
public class AgreeEntity extends BaseEntity {

    /**
     * codeName : 东城区
     * codeValue : dongchengqu
     * id : 360
     * parentId : 359
     * sort : 15
     * status : 0
     * time :
     */
    public List<AreasEntity> areas;
    /**
     * areaId : 360
     * areaName :
     * classDate : 2015-12-24 12:22:29
     * coureseName : 课程1
     * courseId : 1
     * courseImg : /uploadfile/images/2015/12/18/20151218122225-2041524435.png
     * createDate : 2015-12-18 12:22:35
     * id : 1
     * sort : 0
     * status : 0
     * typeId : 377
     * typeName :
     */
    public List<ContentEntity> content;
    /**
     * codeName : 智能机器
     * codeValue : zhinengjiqi
     * id : 379
     * parentId : 376
     * sort : 0
     * status : 0
     * time :
     */
    public List<TypesEntity> types;
}
