package com.mrwang.mystudio.MVP2.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.cinema.artybrigh330.R;

import java.util.ArrayList;

/**
 * ViewPager实现的轮播图广告自定义视图，如京东首页的广告轮播图效果； 既支持自动轮播页面也支持手势滑动切换页面
 */

public class PicLooperView extends RelativeLayout {

    // 自动轮播的时间间隔
    private final static int TIME_INTERVAL = 3;
    // 自动轮播启用开关
    private final static boolean _isAutoPlay = true;
    private ArrayList<Object> bannerList;
    private AutoScrollViewPager viewPager;
    private Context context;
    private CircleIndicator pageIndex;

    //private UserModel userModel;

    public PicLooperView(Context context) {
        this(context, null);
    }

    public PicLooperView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //获取到宽度的模式
        int width_mode = MeasureSpec.getMode(widthMeasureSpec);

        //获取到屏幕的宽度
        int width_size = MeasureSpec.getSize(widthMeasureSpec);

        //高度的大小
        int height_size = 0;
        //说明是填充父窗体
        double scale =203.0/381.00;
        if (width_mode == MeasureSpec.EXACTLY) {
            height_size = (int) (width_size * scale + 0.5f);
        }
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(width_size, MeasureSpec.EXACTLY);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height_size, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setData(ArrayList<Integer> datas) {
        initData(datas);
        initUI(this.context);

        if (_isAutoPlay) {
            viewPager.setInterval(TIME_INTERVAL * 1000);
            viewPager.startAutoScroll();
        }

    }

    public void initData(ArrayList<Integer> datas) {
        bannerList = new ArrayList<>();
        if (null != datas && datas.size() > 0) {
            bannerList.addAll(datas);
        } else {
            bannerList.add(R.mipmap.user);
        }
    }


    public void setDuration(double d) {
        viewPager.setAutoScrollDurationFactor(d);
    }

    /**
     * 点击事件
     */
    private class myClickListener implements OnClickListener {

        private int position;

        public myClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Object o = v.getTag();
            if (null == o) return;
            if (o instanceof Integer) {
                int id = (Integer) o;
            }
        }
    }


    /**
     * 初始化Views等UI
     */
    private void initUI(Context context) {
        viewPager.setAdapter(new MyPagerAdapter(context, bannerList.toArray()));
        pageIndex.setViewPager(viewPager);
        pageIndex.setSelectedPos(0);
    }

    /**
     * 填充ViewPager的页面适配器
     */
    private class MyPagerAdapter extends MyBasePagerAdapter {

        private MyPagerAdapter(Context context, Object[] ivs) {
            super(context, ivs);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            position = (getCount() + position % getCount()) % getCount();
            Object o = objects[position];
            ImageView iv = new ImageView(context);
            if (o instanceof Integer) {
                iv.setTag(o);
                iv.setOnClickListener(new myClickListener(position));
            //} else {
                iv = new ImageView(context);
                iv.setImageResource((Integer) o);
            }
            container.addView(iv);
            return iv;
        }

    }

    @Override
    protected void onFinishInflate() {
        if (!this.isInEditMode()) {
            // 1.Viewpager
            viewPager = (AutoScrollViewPager)findViewById(R.id.vv_home);
            viewPager.setFocusable(true);

            // 2. 圆点
            pageIndex = (CircleIndicator) findViewById(R.id.pageIndexor);
            pageIndex.setDotMargin(10);
            pageIndex.setPaddingBottom(6);
        }
        super.onFinishInflate();
    }


    private int downX;
    private int downY;

    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //让当前viewpager 对应的夫控件不要去拦截事件
                getParent().requestDisallowInterceptTouchEvent(true);
                downX = (int) ev.getX();
                downY = (int) ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                int moveX = (int) ev.getX();
                int moveY = (int) ev.getY();

                //刷新
                if (Math.abs(moveY - downY) > Math.abs(moveX - downX)) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                } else {
                    //滚动轮播图片
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

}