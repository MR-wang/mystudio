package com.mrwang.mystudio.MVP2.base;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.cinema.artybrigh330.MainActivity;
import com.cinema.artybrigh330.R;
import com.cinema.artybrigh330.model.Config;
import com.cinema.artybrigh330.module.IView.Vu;
import com.cinema.artybrigh330.module.ui.Login;
import com.cinema.artybrigh330.utils.SPUtils;
import com.cinema.artybrigh330.utils.UIUtils;
import com.zhy.autolayout.AutoLayout;


/**
 */
@SuppressLint("NewApi")
public abstract class BasePresenterFragment<V extends Vu> extends Fragment implements Handler.Callback, View.OnClickListener {

    protected V vu;
    private ProgressDialog progressDialog;


    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=null;
        try {
            vu = getVuClass().newInstance();
            vu.init(this);
            onBindVu();
            view = vu.getView();
            AutoLayout.getInstance().auto(getActivity(), true);
        } catch (java.lang.InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        UIUtils.getContext().setCallback(this);
        initData();
    }

    protected abstract Class<V> getVuClass();

    public abstract void initData();

    /**
     * 设置控件点击事件
     *
     * @param views views数组
     */
    public void setOnClickListener(View... views) {
        for (View v : views) {
            if (v != null) {
                v.setOnClickListener(this);
            }
        }
    }

    @Override
    public final void onDestroyView() {
        onDestroyVu();
        vu = null;
        super.onDestroyView();
    }

    protected void onDestroyVu() {
    }

    protected void onBindVu() {
    }



    @Override
    public boolean handleMessage(Message msg) {
        return false;
    }

    @Override
    public void onClick(View v) {

    }

    public MainActivity getRoot(){
        return (MainActivity) getActivity();
    }

    protected final SparseArray<View> mViews = new SparseArray<>();

    public void showProgressDialog(String msg) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        try {
            progressDialog = new ProgressDialog(this.getContext());
            progressDialog.setMessage(msg);
            progressDialog.show();
        } catch (WindowManager.BadTokenException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * 隐藏正在加载的进度条
     */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void onNewIntent(){

    }

    public boolean isLogin(){
        boolean isLogin = SPUtils.getInstance().getInt("isLogin") == Config.INSTANCE.LOGIN;
        if (!isLogin){
            AlertDialog.Builder builder=new AlertDialog.Builder(this.getRoot(), R.style.MyAlertDialogStyle);
            builder.setTitle("提示");
            builder.setMessage("亲,请先登陆");
            builder.setPositiveButton("登陆", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    getRoot().popFragment(BasePresenterFragment.this,new Login());
                }
            }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
        return isLogin;
    }



}