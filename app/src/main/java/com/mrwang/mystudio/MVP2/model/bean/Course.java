package com.mrwang.mystudio.MVP2.model.bean;

/**
 * 课程表
 */
public class Course {
	
    public int id;

    public String name;		//课程名称

    public String timelength;	//课程时长

    public String videourl;	//视频地址

    public String enclosure;	//附件

    public String description;	//描述

    public int price;		//价格

    public String createDate;	//创建时间

    public String createUser;	//上传者

    public String typeName;	//类型名称

    public int typeId;		//类型
}