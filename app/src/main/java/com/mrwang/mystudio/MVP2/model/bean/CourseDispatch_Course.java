package com.mrwang.mystudio.MVP2.model.bean;

/**
 * 课程详情的javaBean
 */
public class CourseDispatch_Course {

    public Integer id;            //排课主键

    public Integer courseId;    //课程id

    public String coureseName; //课程名称

    public String courseImg;    //课程封面图

    public String timelength;    //课程时长

    public String videourl;    //视频地址

    public String description;    //描述

    public Integer price;        //价格

    public Integer typeId;        //课程类型id

    public String typeName;    //类型名称

    public Integer areaId;        //区域id

    public String areaName;    //区域名

    public String place;        //上课地点

    public String classDate;    //上课时间

    public String startDate;    //开始时间

    public String endDate;        //结束时间

    public String createDate;    //创建时间

    public String sort;        //排序

    public Integer status;        //状态
}
