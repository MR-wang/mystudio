package com.mrwang.mystudio.MVP2.model.bean;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-12-15
 * Time: 19:44
 */
public class LoginEntity extends BaseEntity{
    /**
     * result : 1 成功 2 失败
     * content : {"area":"东城","bankNumber":"","createDate":"2015-11-25 11:08:48.0","id":1,"idNumber":"1310821659854789955","integral":0,"level":2,"passWord":"","phone":"18801336203","realName":"甄伟明","status":1,"teachstate":1}
     */
    /**
     * area : 东城
     * bankNumber :
     * createDate : 2015-11-25 11:08:48.0
     * id : 1
     * idNumber : 1310821659854789955
     * integral : 0
     * level : 2
     * passWord :
     * phone : 18801336203
     * realName : 甄伟明
     * status : 1
     * teachstate : 1
     */
    public User content;

}
