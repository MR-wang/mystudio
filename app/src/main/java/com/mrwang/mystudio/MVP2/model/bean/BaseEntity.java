package com.mrwang.mystudio.MVP2.model.bean;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-12-21
 * Time: 22:01
 */
public class BaseEntity {
    /**
     * result : 1 成功 2 失败
     */
    public int result;

    public String message;
}  
