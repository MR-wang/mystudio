package com.mrwang.mystudio.MVP2.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2016-01-02
 * Time: 18:05
 */
public class OrderListEntity extends BaseEntity {

    /**
     * classDate : 2015-12-31
     * courseDispatchId : 1
     * courseId : 1
     * courseName : 课程1
     * createDate : 2015-12-31 11:32:08
     * endDate : 21:30
     * id : 5
     * orderNumber : 14515327281054
     * phone : 13241049370
     * place : 朝阳三小五年级二班
     * price : 100
     * startDate : 21:00
     * status : 0
     * userId : 4
     */

    public List<Order> content;

    public static class Order implements Parcelable {

        public String classDate;
        public int courseDispatchId;
        public int courseId;
        public String courseName;
        public String createDate;
        public String endDate;
        public int id;
        public String orderNumber;
        public String phone;
        public String place;
        public String price;
        public String startDate;
        public int status;
        public int userId;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.classDate);
            dest.writeInt(this.courseDispatchId);
            dest.writeInt(this.courseId);
            dest.writeString(this.courseName);
            dest.writeString(this.createDate);
            dest.writeString(this.endDate);
            dest.writeInt(this.id);
            dest.writeString(this.orderNumber);
            dest.writeString(this.phone);
            dest.writeString(this.place);
            dest.writeString(this.price);
            dest.writeString(this.startDate);
            dest.writeInt(this.status);
            dest.writeInt(this.userId);
        }

        public Order() {
        }

        protected Order(Parcel in) {
            this.classDate = in.readString();
            this.courseDispatchId = in.readInt();
            this.courseId = in.readInt();
            this.courseName = in.readString();
            this.createDate = in.readString();
            this.endDate = in.readString();
            this.id = in.readInt();
            this.orderNumber = in.readString();
            this.phone = in.readString();
            this.place = in.readString();
            this.price = in.readString();
            this.startDate = in.readString();
            this.status = in.readInt();
            this.userId = in.readInt();
        }

        public static final Parcelable.Creator<Order> CREATOR = new Parcelable.Creator<Order>() {
            public Order createFromParcel(Parcel source) {
                return new Order(source);
            }

            public Order[] newArray(int size) {
                return new Order[size];
            }
        };
    }
}
