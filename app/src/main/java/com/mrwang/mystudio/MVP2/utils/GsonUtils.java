package com.mrwang.mystudio.MVP2.utils;

import com.google.gson.Gson;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-05-26
 * Time: 15:05
 */
public class GsonUtils {

    private static Gson gson;

    public static <T> T jsonToBean(String json,Class<T> clazz){
        if (gson==null){
            gson = new Gson();
        }
        return gson.fromJson(json, clazz);
    }
}  
