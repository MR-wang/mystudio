package com.mrwang.mystudio.MVP2.model.bean;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-12-09
 * Time: 23:20
 */
public class RegisterUser {
    public RegisterUser(String userName, String passWord, String phone) {
        this.userName = userName;
        this.passWord = passWord;
        this.phone = phone;
    }

    public String userName;
    public String passWord;
    public String phone;
}  
