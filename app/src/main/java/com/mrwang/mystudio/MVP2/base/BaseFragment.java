package com.mrwang.mystudio.MVP2.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.mrwang.mystudio.MVP1.MainActivity;
import com.mrwang.mystudio.MVP1.utils.LogUtils;
import com.mrwang.mystudio.MVP1.utils.UIUtils;


/**
 * Fragment的基类
 */
public abstract class BaseFragment extends Fragment implements Handler.Callback, View.OnClickListener {
    protected View mContentView;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView=initView();
        return mContentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        UIUtils.getContext().setCallback(this);
        initData();
    }

    public MainActivity getRoot(){
        return (MainActivity) getActivity();
    }

    /**
     * 初始化布局
     */
    protected abstract View initView();

    /**
     * 加载数据
     */
    protected abstract void initData();

    @Override
    public boolean handleMessage(Message msg) {
        LogUtils.i("是否执行");
        return false;
    }

    /**
     * 设置控件点击事件
     *
     * @param views views数组
     */
    public void setOnClickListener(View... views) {
        for (View v : views) {
            if (v != null) {
                v.setOnClickListener(this);
            }
        }
    }

    /**
     * 设置控件点击事件
     *
     * @param ids 控件id
     */
    public void setOnClickListenerById(int... ids) {
        for (int id : ids) {
            mContentView.findViewById(id).setOnClickListener(this);
        }
    }

    public void showProgressDialog(String msg) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        try {
            progressDialog = new ProgressDialog(this.getContext());
            progressDialog.setMessage(msg);
            progressDialog.show();
        } catch (WindowManager.BadTokenException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * 隐藏正在加载的进度条
     */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onClick(View v) {

    }

}
