package com.mrwang.mystudio.MVP2.model.bean;

import java.util.List;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-12-21
 * Time: 22:08
 */
public class SelectClassEntity extends BaseEntity {

    /**
     * codeName : 智能机器
     * codeValue : zhinengjiqi
     * id : 379
     * parentId : 376
     * sort : 0
     * status : 0
     * time :
     */

    public List<TypesEntity> types;
    /**
     * codeName : 东城区
     * codeValue : dongchengqu
     * id : 360
     * parentId : 359
     * sort : 15
     * status : 0
     * time :
     */

    public List<AreasEntity> areas;
    public List<?> content;

    public static class TypesEntity {
        public String codeName;
        public String codeValue;
        public int id;
        public int parentId;
        public int sort;
        public int status;
        public String time;
    }

    public static class AreasEntity {
        public String codeName;
        public String codeValue;
        public int id;
        public int parentId;
        public int sort;
        public int status;
        public String time;
    }
}
