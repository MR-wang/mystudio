package com.mrwang.mystudio.MVP2.model.bean;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-12-26
 * Time: 22:09
 */
public class CourseEntity extends BaseEntity {

    /**
     * createDate : 2015-12-15 18:44:21
     * createUser : zhenweiming
     * description : 阿萨德发地方
     * enclosure :
     * id : 1
     * name : 课程1
     * price : 100
     * sort : 1
     * timelength : 25
     * typeId : 377
     * typeName : 恐龙课程
     * videourl : /uploadfile/images/2015/12/15/20151215184425-1701372130.ico
     */
    public Course content;
}
