package com.mrwang.mystudio.MVP2.base;

import android.os.Looper;
import android.os.Message;

import java.lang.ref.WeakReference;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-11-20
 * Time: 20:07
 */
public class BaseHandler extends android.os.Handler {

    private WeakReference<Callback> reference;

    public BaseHandler() {

    }

    public BaseHandler(Looper looper){
        super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
        //LogUtils.i("收到消息");
        if (reference!=null){
            Callback callback = reference.get();
            if (callback!=null){
                callback.handleMessage(msg);
            }
        }
    }

    public void setCallBack(Callback callBack) {
       this.reference= new WeakReference<>(callBack);
    }
}
