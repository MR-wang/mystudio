package com.mrwang.mystudio.MVP2.model.bean;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-12-21
 * Time: 23:41
 */
public class TypesEntity {
    public String codeName;
    public String codeValue;
    public int id;
    public int parentId;
    public int sort;
    public int status;
    public String time;
}  
