package com.mrwang.mystudio.MVP2.model.bean;

public class CourseDispatchUser {

    public Integer id;                    //主键

    public Integer courseDispatchId;    //排课表主键

    public Integer courseId;            //课程id

    public String coureseName;            //课程名

    public Integer userId;            //老师主键

    public String phone;                //老师电话

    public String createDate;            //创建时间

    public Integer status;                //是否下过订单(0，未下单；1，已下单)
    public String courseImg;            //课程宣传图

}
