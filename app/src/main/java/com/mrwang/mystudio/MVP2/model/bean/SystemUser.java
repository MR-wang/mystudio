/**
 *
 */
package com.mrwang.mystudio.MVP2.model.bean;

/**
 * 系统管理员表
 * @author 甄伟明
 * @JDK version used 8.0
 */
public class SystemUser {
    public int id;            //id
    public String userName;    //用户名
    public String passWord;    //密码
    public int status;        //状态{0，失效；1，正常}
    public int roleId;        //角色
    public String createDate;    //时间
}
