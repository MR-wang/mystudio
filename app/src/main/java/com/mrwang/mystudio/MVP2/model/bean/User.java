package com.mrwang.mystudio.MVP2.model.bean;

/**
 * 用户表
 *
 * @author 甄伟明
 * @JDK version used 8.0
 */
public class User {

    public User() {
    }

    public User(String phone, String passWord) {
        this.phone = phone;
        this.passWord = passWord;
    }

    public int id;            //用户id

    public String phone;        //注册账号（手机号）

    public String passWord;    //密码

    public String realName;    //真实姓名

    public String area;        //区域

    public int level;        //用户级别（1，铜牌讲师；2，银牌讲师；3，金牌讲师；）

    public String idNumber;    //身份证号

    public String createDate;    //创建时间

    public int status;        //状态（0，禁用；1，正常）

    public int teachstate;    //能否上课开关（0，不能；1，能）

    public int integral;    //银行卡号

    public String bankNumber;    //用户积分
}