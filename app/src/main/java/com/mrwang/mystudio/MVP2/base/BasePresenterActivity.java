package com.mrwang.mystudio.MVP2.base;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;



/**
 */
public abstract class BasePresenterActivity<V extends Vu> extends Activity {

    protected V vu;

    @SuppressLint("NewApi")
	@Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            vu = getVuClass().newInstance();
            vu.init(null);//TODO 这里传空 暂时不需要用到
            setContentView(vu.getView());
            onBindVu();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected final void onDestroy() {
        onDestroyVu();
        vu = null;
        super.onDestroy();
    }

    @Override
    public final void onBackPressed() {
        if(!handleBackPressed()) {
            super.onBackPressed();
        }
    }

    public boolean handleBackPressed(){
        return false;
    }

    protected abstract Class<V> getVuClass();

    protected void onBindVu(){}

    protected void onDestroyVu() {}

}
