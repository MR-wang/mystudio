package com.mrwang.mystudio.MVP2.base;

import android.app.Activity;
import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import java.util.ArrayList;
import java.util.List;

public class BaseApplication extends Application {

    // 获取到主线程的上下文
    private static BaseApplication mContext = null;

    // 获取到主线程的handler
    public static BaseHandler mMainThreadHandler;
    // 获取到主线程
    private static Thread mMainThread = null;
    // 获取到主线程的id
    private static int mMainThreadId;
    // 获取到主线程的looper
    private static Looper mMainThreadLooper = null;
    public List<Activity> unDestroyActivityList = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        mMainThreadHandler = new BaseHandler();
        mMainThread = Thread.currentThread();
        mMainThreadId = android.os.Process.myTid();
        mMainThreadLooper = getMainLooper();
        init();
    }

    private void init() {
//        OkHttpClient okHttpClient = new OkHttpClient();
//        ImagePipelineConfig config = OkHttpImagePipelineConfigFactory
//                .newBuilder(this, okHttpClient)
//                .build();
//        Fresco.initialize(this, config);
//        Stetho.initialize(
//                Stetho.newInitializerBuilder(this)
//                        .enableDumpapp(
//                                Stetho.defaultDumperPluginsProvider(this))
//                        .enableWebKitInspector(
//                                Stetho.defaultInspectorModulesProvider(this))
//                        .build());
    }

    // 对外暴露上下文
    public static BaseApplication getApplication() {
        return mContext;
    }

    // 对外暴露主线程的handler
    public static Handler getMainThreadHandler() {
        return mMainThreadHandler;
    }

    // 对外暴露主线程
    public static Thread getMainThread() {
        return mMainThread;
    }

    // 对外暴露主线程id
    public static int getMainThreadId() {
        return mMainThreadId;
    }

    // 对外暴露主线程的looper
    public static Looper getMainThreadLooper() {
        return mMainThreadLooper;
    }

    public void setCallback(Handler.Callback callback) {
        mMainThreadHandler.setCallBack(callback);
    }

    /**
     * 退出应用
     */
    public void quit() {
        for (Activity activity : unDestroyActivityList) {
            if (null != activity) {
                activity.finish();
            }
        }
        unDestroyActivityList.clear();
    }
}
