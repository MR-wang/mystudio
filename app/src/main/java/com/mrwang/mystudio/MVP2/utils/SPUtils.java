
package com.mrwang.mystudio.MVP2.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.cinema.artybrigh330.model.Config;
import com.cinema.artybrigh330.model.bean.User;


/**
 * SharedPreferences实体类
 */
public class SPUtils {
    /**
     * SharedPreferences的名字
     */
    private static final String SP_FILE_NAME = "APPLICATION_SP";
    public static SPUtils sharedPrefHelper;
    public static SharedPreferences sharedPreferences;

    public static synchronized SPUtils getInstance() {
        if (null == sharedPrefHelper) {
            sharedPrefHelper = new SPUtils();
        }
        return sharedPrefHelper;
    }

    private SPUtils() {
        sharedPreferences = UIUtils.getContext().getSharedPreferences(SP_FILE_NAME, Context.MODE_PRIVATE);
    }


    public  void saveString(String key, String msg) {
        sharedPreferences.edit().putString(key, msg).apply();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public  void saveBoolean(String key, boolean msg) {
        sharedPreferences.edit().putBoolean(key, msg).apply();
    }

    public  boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }
    public  void saveInt(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public  int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }



    public  void saveLong(String key, long value) {
        sharedPreferences.edit().putLong(key, value).apply();
    }

    public  long getLong(String key) {
        return sharedPreferences.getLong(key, 0);
    }

    public boolean isLogin() {
        return sharedPreferences.getBoolean("loginStatus", false);
    }

    public int getUserId(){
        int id;
        if (getInt("isLogin") == Config.INSTANCE.LOGIN) {
            id = getInt("id");
        } else {
            id = getInt("anonymityId");
        }
        return id;
    }


    public Long getLastPlayPosition(int movieId) {
        return sharedPreferences.getLong(getLoginId() + "_" + movieId, 0L);
    }

    public Integer getLoginId() {
        return sharedPreferences.getInt("id", 0);
    }
    public void setLastPlayPositionByUserAndMovie(int movieId, long position) {
        sharedPreferences.edit().putLong(getLoginId() + "_" + movieId, position).apply();
    }

    public void saveUser(User user) {
        saveInt("id",user.id);
        saveString("phone", user.phone);
        saveString("passWord", user.passWord);
    }
}
