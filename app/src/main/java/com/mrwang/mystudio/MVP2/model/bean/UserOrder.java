package com.mrwang.mystudio.MVP2.model.bean;

import java.util.Date;

/**
 * 订单表
 */
public class UserOrder {
	
    public int id;				//自增id

    public int userId;			//用户id（外键）

    public int courseId;		//课程id

    public String courseName;		//课程名称

    public String orderNumber;		//订单号

    public String place;			//地点

    public Double price;			//价格

    public Date createDate;		//创建时间

    public int status;			//订单状态(0，未接单；1，已结单；2，已退单，3，订单完成；4，结算完毕）

}