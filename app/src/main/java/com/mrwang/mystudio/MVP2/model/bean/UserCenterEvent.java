package com.mrwang.mystudio.MVP2.model.bean;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-12-08
 * Time: 23:13
 */
public class UserCenterEvent {
    public boolean isRefresh;

    public UserCenterEvent(boolean isRefresh) {
        this.isRefresh = isRefresh;
    }
}
