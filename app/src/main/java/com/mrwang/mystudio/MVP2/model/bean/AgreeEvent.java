package com.mrwang.mystudio.MVP2.model.bean;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2016-01-02
 * Time: 17:03
 */
public class AgreeEvent {
    public boolean isRefresh;

    public AgreeEvent(boolean isRefresh) {
        this.isRefresh = isRefresh;
    }
}
