package com.mrwang.mystudio.MVP2.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-09-29
 * Time: 14:52
 */
public class BaseRVHolder  extends RecyclerView.ViewHolder {


    private ViewHolder viewHolder;

    public BaseRVHolder(View itemView) {
        super(itemView);
        viewHolder= ViewHolder.getViewHolder(itemView);
    }


    public ViewHolder getViewHolder() {
        return viewHolder;
    }

}
