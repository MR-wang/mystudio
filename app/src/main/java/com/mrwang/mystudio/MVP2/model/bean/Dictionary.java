/**
 *
 */
package com.mrwang.mystudio.MVP2.model.bean;

/**
 * 字典表
 *
 * @author 甄伟明
 * @JDK version used 8.0
 */
public class Dictionary {
    public int id;            //id
    public int parentId;    //父级id
    public String codeValue;    //码值
    public String codeName;    //名称
    public int status;        //状态{0，失效；1，正常}
    public String time;        //时间
}
