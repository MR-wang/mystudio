package com.mrwang.mystudio.MVP2.model.bean;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-12-21
 * Time: 23:41
 */
public class ContentEntity {
    public int areaId;
    public String areaName;
    public String classDate;
    public String coureseName;
    public int courseId;
    public String courseImg;
    public String createDate;
    public int id;
    public String sort;
    public int status;
    public int typeId;
    public String typeName;
}
