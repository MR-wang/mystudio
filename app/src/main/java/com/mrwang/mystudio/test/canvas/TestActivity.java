package com.mrwang.mystudio.test.canvas;

import android.app.Activity;
import android.os.Bundle;

import com.mrwang.mystudio.R;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2016-02-02
 * Time: 15:23
 */
public class TestActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.canvas_test2);
    }
}
