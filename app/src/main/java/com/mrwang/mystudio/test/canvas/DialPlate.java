package com.mrwang.mystudio.test.canvas;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2016-02-02
 * Time: 15:30
 */
public class DialPlate extends View {

    public DialPlate(Context context) {
        super(context);
    }

    public DialPlate(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DialPlate(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
