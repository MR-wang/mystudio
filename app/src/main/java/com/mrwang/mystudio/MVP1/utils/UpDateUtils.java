package com.mrwang.mystudio.MVP1.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;

/**
 * 应用更新的工具类
 * 调用系统的方法 更稳定 更轻量
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-09-10
 * Time: 17:58
 */
public enum  UpDateUtils {
    INSTANCE;

    /**
     * 应用更新
     * @param path 下载地址
     * @param destination 通知栏描述
     * @param apkName apk的名字
     * @param onlyWIFI 是否只在WIFI环境下下载
     */
    public void upDate(String path,String destination,String apkName,boolean onlyWIFI){
        //String path = "http://img.itjh.com.cn/VCinemaV3.apk";
        String filePath=UIUtils.getContext().getExternalCacheDir() + "/upDate";
        FileUtils.createDirs(filePath);
        DownloadManager dManager = (DownloadManager)UIUtils.getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(path);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        // 设置下载路径和文件名
        request.setDestinationInExternalPublicDir("upDate", apkName);
        request.setDescription(destination);
        //设置下载完成后显示通知栏
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        int networkAvailable = SystemUtils.getNetworkAvailable();
        if (networkAvailable==0){
            UIUtils.showCententToast("当前网络不可用,请检查网络");
            return;
        }
        if (onlyWIFI){
            if (networkAvailable==2){//说明是移动网络
                UIUtils.showCententToast("当前是移动网络,建议切换到WIFI网络进行下载");
            }else{
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
            }

        }
        //设置下载文件的mineType 文件类型
        request.setMimeType("application/vnd.android.package-archive");
        // 设置为可被媒体扫描器找到
        request.allowScanningByMediaScanner();
        // 设置为可见和可管理
        request.setVisibleInDownloadsUi(true);
        long refernece = dManager.enqueue(request);
        // 把当前下载的ID保存起来
        SPUtils.getInstance().saveLong("refernece", refernece);
    }

    /*
        相关API
     * request.allowScanningByMediaScanner();表示允许MediaScanner扫描到这个文件，默认不允许。
     * request.setTitle(“MeiLiShuo”);设置下载中通知栏提示的标题
     * request.setDescription(“MeiLiShuo desc”);设置下载中通知栏提示的介绍
     * request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
     * 表示下载进行中和下载完成的通知栏是否显示。默认只显示下载中通知。VISIBILITY_VISIBLE_NOTIFY_COMPLETED表示下载完成后显示通知栏提示。
     * VISIBILITY_HIDDEN表示不显示任何通知栏提示，这个需要在AndroidMainfest中添加权限android.permission.DOWNLOAD_WITHOUT_NOTIFICATION.
     * <p/>
     * request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
     * 表示下载允许的网络类型，默认在任何网络下都允许下载。有NETWORK_MOBILE、NETWORK_WIFI、NETWORK_BLUETOOTH三种及其组合可供选择。如果只允许wifi下载，而当前网络为3g，则下载会等待。
     * request.setAllowedOverRoaming(boolean allow)移动网络情况下是否允许漫游。
     * <p/>
     * request.setMimeType(“application/cn.trinea.download.file”);
     * 设置下载文件的mineType。因为下载管理Ui中点击某个已下载完成文件及下载完成点击通知栏提示都会根据mimeType去打开文件，所以我们可以利用这个属性。比如上面设置了mimeType为application/cn.trinea.download.file，我们可以同时设置某个Activity的intent-filter为application/cn.trinea.download.file，用于响应点击的打开文件。
     */
}  
