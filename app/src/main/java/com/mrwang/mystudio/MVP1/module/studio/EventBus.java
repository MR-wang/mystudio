package com.mrwang.mystudio.MVP1.module.studio;

/**
 * 自定义的eventBus
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-05-15
 * Time: 13:59
 */
public class EventBus {
    private static EventBus eventBus;

    private EventBus(){
    }

    public static EventBus getDefult(){
        if (null==eventBus){
            eventBus=new EventBus();
        }
        return eventBus;
    }
}

