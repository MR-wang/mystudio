package com.mrwang.mystudio.MVP1.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.List;

/**
 * 泡网 http://www.jcodecraeer.com/a/anzhuokaifa/androidkaifa/2015/0804/3259.html
 * 我们新写的适配器继承AutoRVAdapter 实现onCreateViewLayoutID 和 onBindViewHolder 方法即可.
    onCreateViewLayoutID->返回item 的布局.
    onBindViewHolder->绑定数据源.
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-09-29
 * Time: 14:48
 */
public abstract class BaseRVAdapter extends RecyclerView.Adapter<BaseRVHolder> {


    public List<?> list;

    private Context context;

    public BaseRVAdapter(Context context, List<?> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public BaseRVHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(onCreateViewLayoutID(viewType), null);

        return new BaseRVHolder(view);
    }

    public abstract int onCreateViewLayoutID(int viewType);


    @Override
    public void onViewRecycled(final BaseRVHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public void onBindViewHolder(final BaseRVHolder holder, final int position) {

        onBindViewHolder(holder.getViewHolder(), position);
        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(null, v, holder.getPosition(), holder.getItemId());
                }
            });
        }

    }

    public abstract void onBindViewHolder(ViewHolder holder, int position);

    @Override
    public int getItemCount() {
        return list.size();
    }

    private AdapterView.OnItemClickListener onItemClickListener;

    public AdapterView.OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
