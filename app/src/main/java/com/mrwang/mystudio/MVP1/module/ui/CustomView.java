package com.mrwang.mystudio.MVP1.module.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-09-11
 * Time: 13:44
 * 使用内部类 自定义一个简单的View
 * @author Administrator
 *
 */
public class CustomView extends View {

    Paint paint;

    public CustomView(Context context) {
        super(context);
        paint = new Paint(); //设置一个笔刷大小是3的黄色的画笔
        paint.setColor(Color.YELLOW);//设置画笔颜色
        paint.setStrokeJoin(Paint.Join.ROUND);//设置绘制时个图形的结合方式，如平滑效果等，MITER为锐角，ROUND为圆弧,BEVEL结合处为直线
        paint.setStrokeCap(Paint.Cap.ROUND);//该方法用来设置我们画笔的 笔触风格 设置笔触为园笔
        paint.setStrokeWidth(3);//当画笔样式为STROKE或FILL_OR_STROKE时，设置笔刷的粗细度
    }

    //在这里我们将测试canvas提供的绘制图形方法
    @Override
    protected void onDraw(Canvas canvas) {
        //弧线(arcs)、填充颜色(argb和color)、 Bitmap、圆(circle和oval)、
        // 点(point)、线(line)、矩形(Rect)、图片(Picture)、圆角矩形 (RoundRect)、
        // 文本(text)、顶点(Vertices)、路径(path)
        canvas.drawCircle(100, 100, 90, paint);

    }

}