package com.mrwang.mystudio.MVP1;

import android.view.View;
import android.widget.ProgressBar;

import com.mrwang.mystudio.MVP1.base.BaseActivity;
import com.mrwang.mystudio.R;
import com.mrwang.mystudio.MVP1.utils.UpDateUtils;

import java.io.File;


/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-07-13
 * Time: 11:01
 */
public class MainActivity extends BaseActivity {

    private ProgressBar progressBar;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_main);
        //progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    protected void initDate() {

    }

    public void click(View view) {
        String path = "http://img.itjh.com.cn/VCinemaV3.apk";
        UpDateUtils.INSTANCE.upDate(path,"V影院更新..","VCinemaV3.apk",true);
    }


    public boolean createDir(String Path) {
        File folder = new File(Path);
        return (folder.exists() && folder.isDirectory()) || folder.mkdirs();
    }
}
