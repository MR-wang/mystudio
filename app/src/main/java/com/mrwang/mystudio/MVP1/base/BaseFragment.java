package com.mrwang.mystudio.MVP1.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mrwang.mystudio.R;
import com.mrwang.mystudio.MVP1.utils.UIUtils;

import java.util.List;

/**
 * Fragment的基类
 */
public abstract class BaseFragment extends Fragment {
    protected LoadingPage mContentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //每次ViewPager要展示该页面时，均会调用该方法获取显示的View
        if (mContentView == null) {//为null时，创建一个
            mContentView = new LoadingPage(UIUtils.getContext()) {
                @Override
                public LoadResult load() {
                    return BaseFragment.this.load();
                }

                @Override
                public View createLoadedView() {
                    return BaseFragment.this.createLoadedView();
                }
            };
        }
        return mContentView;
    }

    /**
     * 当显示的时候，加载该页面
     */
    public void show() {
        if (mContentView != null) {
            mContentView.show();
        }
    }

    /**
     * 检查数据正确性 每个项目均有不同
     */
    public LoadingPage.LoadResult check(Object obj) {
        if (obj == null) {
            return LoadingPage.LoadResult.ERROR;
        }
        if (obj instanceof List) {
            List list = (List) obj;
            if (list.size() == 0) {
                return LoadingPage.LoadResult.EMPTY;
            }
        }
        return LoadingPage.LoadResult.SUCCEED;
    }

    /**
     * 加载数据 已在子线程中操作
     */
    protected abstract LoadingPage.LoadResult load();

    /**
     * 加载完成的View
     */
    protected abstract View createLoadedView();

    /**
     * 添加fragment，并带动画切换效果
     * @param id 要替换的布局id
     * @param fragment 替换此布局的fragment
     */
    public void addFragment(int id,Fragment fragment){
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.next_in,R.anim.next_out);
        ft.replace(id,fragment);
        ft.commit();
    }
}
