package com.mrwang.mystudio.MVP1.module.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mrwang.mystudio.MVP1.utils.UIUtils;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-09-05
 * Time: 00:48
 */
public class HomeFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        TextView textView=new TextView(UIUtils.getContext());
        textView.setText("首页，最底层的页面");
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //弹出页面
                ((FragActivity) getActivity()).popFragmant(HomeFragment.this, new Fragment1());
            }
        });

        return textView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
