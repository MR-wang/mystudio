package com.mrwang.mystudio.MVP1.module.ui;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;

import com.mrwang.mystudio.R;

/**
 * Canvas使用详解
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-09-11
 * Time: 13:42
 */
public class CanvasDemoActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.canvas_test);
    }



}
