package com.mrwang.mystudio.MVP1.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2016-02-02
 * Time: 14:18
 */
public class PhoneUtils {
    /**
     * 拨打电话
     */
    public static void call(Context context, String phoneNumber) {
        //注意要添加权限<uses-permission android:name="android.permission.READ_PROFILE" />
        //<uses-permission android:name="android.permission.READ_CONTACTS" />
        context.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber)));
    }

    /**
     * 跳转至拨号界面
     */
    public static void callDial(Context context, String phoneNumber) {
        context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber)));
    }

    /**
     * 发送短信
     */
    public static void sendSms(Context context, String phoneNumber,
                               String content) {
        Uri uri = Uri.parse("smsto:"
                + (TextUtils.isEmpty(phoneNumber) ? "" : phoneNumber));
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", TextUtils.isEmpty(content) ? "" : content);
        context.startActivity(intent);
    }


}  
