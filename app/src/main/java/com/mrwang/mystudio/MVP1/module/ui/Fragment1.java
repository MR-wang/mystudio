package com.mrwang.mystudio.MVP1.module.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mrwang.mystudio.R;
import com.mrwang.mystudio.MVP1.utils.UIUtils;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-09-05
 * Time: 00:48
 */
public class Fragment1 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = UIUtils.inflate(R.layout.frag1);
        TextView textView2 = (TextView) inflate.findViewById(R.id.textView3);
        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //以dialog的方式弹出页面
                ((FragActivity)getActivity()).popFragmant(new Fragment2());
            }
        });
        return inflate;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
