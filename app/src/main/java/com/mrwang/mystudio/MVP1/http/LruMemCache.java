package com.mrwang.mystudio.MVP1.http;

import android.util.LruCache;

/**
 * 将请求结果缓存到内存中
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-07-15
 * Time: 12:07
 */
public class LruMemCache implements Cache<String, Response>{
    /**
     * Reponse缓存
     */
    private LruCache<String, Response> mResponseCache;

    public LruMemCache() {

        // 计算可使用的最大内存
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        // 取八分之一的可用内存作为缓存
        final int cacheSize = maxMemory / 10;
        mResponseCache = new LruCache<String, Response>(cacheSize) {

            @Override
            protected int sizeOf(String key, Response response) {
                return response.result.length() / 1024;
            }
        };
    }

    public Response get(String key) {
        return mResponseCache.get(key);
    }

    public void put(String key, Response response) {
        mResponseCache.put(key, response);
    }

    public void remove(String key) {
        mResponseCache.remove(key);
    }
}  
