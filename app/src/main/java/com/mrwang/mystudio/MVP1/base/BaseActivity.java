package com.mrwang.mystudio.MVP1.base;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;

import com.mrwang.mystudio.MVP1.utils.UIUtils;

public abstract class BaseActivity extends FragmentActivity implements Handler.Callback {

    // 获取到前台进程的Activity
    private static Activity mForegroundActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIUtils.getContext().setCallback(this);
        initView();
        initDate();
    }


    /**
     * 初始化界面
     */
    protected abstract void initView();

    /**
     * 初始化数据 此方法一定是可以复用的 刷新数据调用即可
     */
    protected abstract void initDate();


    @Override
    protected void onResume() {
        super.onResume();
        mForegroundActivity = this;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mForegroundActivity = null;
    }

    public static Activity getForegroundActivity() {
        return mForegroundActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UIUtils.getContext().callback=null;//防止内存泄漏
    }

    /**
     * 如需使用handler 只需要重写此方法即可
     */
    @Override
    public boolean handleMessage(Message msg) {
        return true;
    }
}
