package com.mrwang.mystudio.MVP1.widget;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-07-24
 * Time: 23:13
 */
public class MySwipeRefreshLayout extends SwipeRefreshLayout{
    private boolean isIntercept;

    public MySwipeRefreshLayout(Context context) {
        super(context);
    }

    public MySwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (isIntercept){
            return false;
        }else{
            return super.onInterceptTouchEvent(ev);
        }
    }

    public void setIntercept(boolean isIntercept){
        this.isIntercept=isIntercept;
    }

}
