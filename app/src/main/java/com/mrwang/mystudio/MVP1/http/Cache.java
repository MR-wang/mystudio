package com.mrwang.mystudio.MVP1.http;

/**
 * 请求缓存接口
 * @param <K> key的类型
 * @param <V> value类型
 * * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-07-15
 * Time: 12:15
 */
public interface Cache<K, V> {

    public V get(K key);

    public void put(K key, V value);

    public void remove(K key);

}
