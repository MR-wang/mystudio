
package com.mrwang.mystudio.MVP1.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 应用工具类
 *
 * @author Axl.Jacobs
 */
public class AppUtil {

    private static final String TAG = "AppUtil";

    /**
     * 取得屏幕分辨率
     */
    public static Point getScreenResolution(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        return point;
    }


    /**
     * dip转换成px
     *
     * @param context  Context对象
     * @param dipValue 要转换的值
     * @return px值
     */
    public static int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    /**
     * px转换成dip
     *
     * @param context Context对象
     * @param pxValue 要转换的值
     * @return dip值
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * px转sp
     */
    public static int px2sp(Context context, float pxValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }
    /**
     * sp转px
     */
    public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }


    /**
     * 判断手机是否开启GPS模块
     *
     * @param context Context对象
     * @return 是否开启
     */
    public static boolean isOpenGps(Context context) {
        LocationManager alm = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        return alm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    //

    /**
     * 判断手机是否连接网络
     *
     * @param context Context对象
     * @return 是否开启
     */
    public static boolean isConnectNetwork(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        return !(info == null || !info.isConnected());
    }

    /**
     * 判断是否为wifi网络
     *
     * @param context Context对象
     */
    public static int isWifi(Context context) {
        ConnectivityManager connectMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectMgr.getActiveNetworkInfo();
        if (info==null){
            return 0;
        }else if (info.getType() == ConnectivityManager.TYPE_WIFI){
            return 1;
        }else{
            return 2;
        }
    }

    // 非空返回""

    /**
     * 非空返回
     *
     * @param param 要检测的值
     * @return 返回的字符串
     */
    public static String returnDefaultWhenNull(String param) {
        if (AppUtil.isNotBlank(param))
            return param;
        return "不限";
    }


    /**
     * 返回非空信息
     *
     * @param param 要检测的值
     * @return 返回值
     */
    public static String returnZeroIfNull(String param) {
        if (param == null)
            return "0";
        return param;
    }


    /**
     * 获取资源字符串
     *
     * @param context Context对象
     * @param strId   字符串ID
     * @return 获取到的字符串
     */
    public static String getStringFromResouces(Context context, int strId) {
        return context.getResources().getString(strId);
    }


    /**
     * 显示环形进度条
     *
     * @param context     Context对象
     * @param progressBar 进度条对象
     * @return 进度条
     */
    public static ProgressBar showRingProgressBar(Context context,
                                                  ProgressBar progressBar) {
        if (progressBar == null) {
            progressBar = new ProgressBar(context);
            progressBar.setIndeterminate(true);
        }
        progressBar.setVisibility(ProgressBar.VISIBLE);
        return progressBar;
    }

    /**
     * 判断字符串不为空
     */
    public static boolean isNotBlank(String str) {
        return str != null && !"".equals(str) && !"null".equals(str);
    }

    /**
     * 设置文字内容
     *
     * @param str 文字内容
     * @return 内容为空是显示空
     */
    public static String setText(String str) {
        return isNotBlank(str) ? str : "";
    }

    /**
     * 设置文字内容
     *
     * @param str 文字内容
     * @return 内容为空是显示空
     */
    public static String setEncoderText(String str) {
        return isNotBlank(str) ? URLEncoder.encode(str) : "";
    }

    /**
     * 设置文字内容
     *
     * @param str  文字内容
     * @param temp 内容为空时显示文字
     */
    public static String setText(String str, String temp) {
        return isNotBlank(str) ? setText(str) : temp;
    }


    /**
     * Toast显示一条信息
     *
     * @param context Context对象
     * @param msg     显示内容
     */
    public static void showToastMsg(Context context, String msg) {
        AppUtil.showToastMsg(context, msg, 5);
    }

    // Toast显示一条信息
    public static void showToastMsg(Context context, String msg, int time) {
        if (msg != null && !"".equals(msg))
            Toast.makeText(context, msg, time).show();
    }


    /**
     * x字符分割字符串返回字符串数组
     *
     * @param str 字符串
     * @return 字符串数组
     */
    public static String[] splitStrByx(String str) {
        if (AppUtil.isNotBlank(str)) {
            return str.split("x");
        }
        return null;
    }


    /**
     * 关闭加载提示窗
     *
     * @param dialog 对话框
     */
    public static void dismissProgressDialog(ProgressDialog dialog) {
        try {
            dialog.dismiss();
        } catch (IllegalArgumentException e) {
        }
    }

    //

    /**
     * URL中文编码
     *
     * @param param 要编码的字符串
     * @return 编码后的字符串
     */
    public static String encoderBase64Str(String param) {
        if (isNotBlank(param)) {
            param = URLEncoder.encode(param);
            return param;
        }
        return "";
    }


    /**
     * 判断是否存在SDCard
     */
    public static boolean isExistSDCard() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }


    /**
     * 显示对话框进度条
     *
     * @param context Context对象
     * @param dialog  dialog
     * @param title   标题
     * @param message 内容
     * @return 对话框对象
     */
    public static ProgressDialog showProgressDialog(Context context,
                                                    ProgressDialog dialog, int title, int message) {
        String titleString = null;
        if (title > 0)
            titleString = context.getString(title);
        String messageString = context.getString(message);

        return showProgressDialog(context, dialog, titleString, messageString);
    }

    /**
     * 显示对话框进度条
     *
     * @param context Context对象
     * @param dialog  dialog
     * @param title   标题
     * @param message 内容
     * @return 对话框对象
     */
    public static ProgressDialog showProgressDialog(Context context,
                                                    ProgressDialog dialog, String title, String message) {
        if (dialog == null) {
            dialog = new ProgressDialog(context);
            if (title != null)
                dialog.setTitle(title);

            if (message == null)
                message = "加载中...请稍后...";
            dialog.setMessage(message);
            /** 设置进度条不确定 **/
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);//
            dialog.setCanceledOnTouchOutside(false);
        }
        dialog.show();
        return dialog;
    }

    //

    /**
     * 移动动画
     *
     * @param context    Context对象
     * @param view       要移动的view
     * @param fromXDelta X起始点
     * @param toXDelta   X结束点
     * @param fromYDelta Y起始点
     * @param toYDelta   Y结束点
     */
    public static void translateView(Context context, View view,
                                     int fromXDelta, int toXDelta, int fromYDelta, int toYDelta) {
        int pxdistance = Math.abs(toXDelta - fromXDelta);
        int dipdistance = px2dip(context, pxdistance);
        TranslateAnimation tanimation = new TranslateAnimation(fromXDelta,
                toXDelta, fromYDelta, toYDelta);
        if (dipdistance <= 69) {
            tanimation.setDuration(200);
        } else if (dipdistance > 69 && dipdistance <= 138) {
            tanimation.setDuration(400);
        } else if (dipdistance > 138 && dipdistance <= 276) {
            tanimation.setDuration(500);
        } else {
            tanimation.setDuration(600);
        }
        tanimation.setFillAfter(true);
        view.startAnimation(tanimation);
    }

    /**
     * 获取本机手机号
     *
     * @param context Context对象
     * 需求 android.permission.READ_PHONE_STATE 用户权限
     */
    public static String getPhoneNo(Context context) {
        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getLine1Number();
    }

    /**
     * 获取本机 IMEI 号
     *
     * @param context Context对象
     */
    public static String getIMEI(Context context) {
        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getDeviceId();
    }

    /**
     * 获取本机的设备名称
     *
     * @param context Context对象
     */
    public static String getDeviceModel(Context context) {
        return Build.MODEL;
    }

    /**
     * 获取 android 的版本
     *
     * @param context Context对象
     */
    public static String getDeviceVersion(Context context) {
        return "Android_" + VERSION.RELEASE;
    }


    /**
     * 判断网络是否可用
     *
     * @param context Context对象
     */
    public static Boolean isNetworkReachable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo current = cm.getActiveNetworkInfo();
        if (current == null) {
            return false;
        }
        return (current.isAvailable());
    }


    /**
     * 获取MAC地址
     *
     * @param context Context对象
     */
    public static String getLocalMacAddress(Context context) {
        WifiManager wifi = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();
        return info.getMacAddress();
    }

    /**
     * 获取设备唯一标识
     *
     * @param context Context对象
     * @return 标识字符串
     */
    public static String getUniqueIdentification(Context context) {
        String str = "www." + getDeviceModel(context) + getIMEI(context)
                + getLocalMacAddress(context) + "vcinema.cn";
        str = MD5.getMessageDigest(str.getBytes());
        return str;
    }

    /**
     * 获取本机IP地址
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static String getLocalIpAddress() {
        try {
            for (Enumeration en = NetworkInterface.getNetworkInterfaces(); en
                    .hasMoreElements(); ) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (Enumeration enumIpAddr = intf.getInetAddresses(); enumIpAddr
                        .hasMoreElements(); ) {
                    InetAddress inetAddress = (InetAddress) enumIpAddr
                            .nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {

        }
        return "192.168.1.1";
    }

    /**
     * 获取IPV4地址
     */
    public static String getLocalIPV4Address() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()
                            && (inetAddress instanceof Inet4Address)) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return "192.168.1.1";
    }


    public static String getWIFILocalIpAdress(Context mContext) {
        //获取wifi服务
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        //判断wifi是否开启
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipAddress = wifiInfo.getIpAddress();
        String ip = formatIpAddress(ipAddress);
        return ip;
    }

    private static String formatIpAddress(int ipAdress) {

        return (ipAdress & 0xFF) + "." +
                ((ipAdress >> 8) & 0xFF) + "." +
                ((ipAdress >> 16) & 0xFF) + "." +
                (ipAdress >> 24 & 0xFF);
    }


    /***
     * 获取网关IP地址
     *
     * @return
     */
    public static String getHostIp() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> ipAddr = intf.getInetAddresses(); ipAddr
                        .hasMoreElements(); ) {
                    InetAddress inetAddress = ipAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 保留小数点,默认为两位
     *
     * @param formatNum    需要被格式化的数据
     * @param decimalPoint 保留的小数点的位数
     */
    public static String saveNDecimalPlaces(Double formatNum, int decimalPoint) {
        StringBuilder sb = new StringBuilder("0.00");
        if (decimalPoint > 0) {
            sb.delete(0, sb.length());
            sb.append("0.");
            for (int i = 0; i < decimalPoint; i++) {
                sb.append("0");
            }
        }

        DecimalFormat format = new DecimalFormat(sb.toString());
        return format.format(formatNum);
    }

    /**
     * 日期是否相同
     *
     * @param date1 日期源1
     * @param date2 日期源1
     * @return 是否相同
     */
    public static Boolean equalsDate(Date date1, Date date2) {

        return date1.getYear() == date2.getYear()
                && date1.getMonth() == date2.getMonth()
                && date1.getDate() == date2.getDate();
    }

    public static String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        //DateFormat df = DateFormat.getDateTimeInstance ();
        //String formatDate =;
        return formatter.format(curDate);
    }

    /**
     * 重新计算ListViewItem高度,解决listVIew和ScrollView冲突
     *
     * @param listView 要计算的listView
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    /**
     * 全角转换
     *
     * @param input 要转换的字符串
     */
    public static String ToDBC(String input) {
        char[] c = input.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == 12288) {
                c[i] = (char) 32;
                continue;
            }
            if (c[i] > 65280 && c[i] < 65375)
                c[i] = (char) (c[i] - 65248);
        }
        return new String(c);
    }

    /**
     * 图片缩放
     *
     * @param photo     图片
     * @param newHeight 新的高度
     * @param context   Context对象
     * @return 缩放后的图片
     */
    public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight,
                                         Context context) {

        final float densityMultiplier = context.getResources()
                .getDisplayMetrics().density;

        int h = (int) (newHeight * densityMultiplier);
        int w = (int) (h * photo.getWidth() / ((double) photo.getHeight()));

        photo = Bitmap.createScaledBitmap(photo, w, h, true);

        return photo;
    }

    /**
     * 圆角化图片
     */
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
//
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);
        // 得到画布
        Canvas canvas = new Canvas(output);

        // 将画布的四角圆化
        final int color = Color.RED;
        final Paint paint = new Paint();
        // 得到与图像相同大小的区域 由构造的四个值决定区域的位置以及大小
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        // 值越大角度越明显
        final float roundPx = 25;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // drawRoundRect的第2,3个参数一样则画的是正圆的一角，如果数值不同则是椭圆的一角
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    /**
     * 图片圆角
     *
     * @param bitmap 图片的bitmap
     * @param pixels 有效像素
     * @return 圆角图片
     */
    public static Bitmap toRoundCorner(Bitmap bitmap, int pixels) {

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;

        final Paint paint = new Paint();

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        final RectF rectF = new RectF(rect);

        final float roundPx = pixels;

        paint.setAntiAlias(true);

        canvas.drawARGB(0, 0, 0, 0);

        paint.setColor(color);

        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));

        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;

    }

    /**
     * 获取当前应用程序的版本名称
     */
    public static String getVersionName(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            return info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return "版本号未知";
        }
    }

    /**
     * 获取渠道信息
     *
     * @param context 当前的context对象
     * @return 渠道名称
     */
    public static String getChannelNo(Context context) {
        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            return info.metaData.getString("UMENG_CHANNEL");
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 获取当前应用程序的版本号
     *
     * @return
     */
    public static int getVersionCode(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(),
                    0);
            return info.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 去除URL中的空格
     *
     * @param str
     * @return
     */
    public static String UrlEncoder(String str) {
        if (str != null) {
            str = str.replace(" ", "%20");
        }
        return str;
    }

    /**
     * 去除特殊字符或将所有中文标号替换为英文标号
     *
     * @param str
     * @return
     */
    public static String stringFilter(String str) {
        str = str.replaceAll("【", "[").replaceAll("】", "]")
                .replaceAll("！", "!").replaceAll("：", ":");
        // .replaceAll(" ", "20%");// 替换中文标号
        String regEx = "[『』]"; // 清除掉特殊字符
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll("").trim();
    }

    /**
     * 以最省内存的方式读取本地资源的图片
     *
     * @param context
     * @param resId
     * @return
     */
    public static Bitmap readBitMap(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        // 获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    public static Bitmap bytes2Bitmap_compress(byte[] imgData, int scale) {
        if (imgData.length != 0) {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(imgData, 0, imgData.length, opts);
            opts.inSampleSize = scale; // the value you can set bigger than 1.
            opts.inPreferredConfig = Config.RGB_565;
            opts.inJustDecodeBounds = false;
            return BitmapFactory.decodeByteArray(imgData, 0, imgData.length,
                    opts);
        }
        return null;
    }

    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    /**
     * 向文件末尾追加字符串
     *
     * @param file   文件对象
     * @param conent 字符串内容
     */
    public static void addStr(File file, String conent) {
        BufferedWriter out = null;
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                //if (DEBUG)
                e.printStackTrace();
            }
        }
        try {
            out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file, true)));
            out.write(conent);
            out.newLine();
        } catch (Exception e) {
            //if (DEBUG)
            e.printStackTrace();
        } finally {
            try {
                assert out != null;
                out.close();
            } catch (IOException e) {
                //if (DEBUG)
                e.printStackTrace();
            }
        }
    }

    /**
     * 拷贝文件
     *
     * @param is 输入流
     * @param os 输出流
     */
    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }


    /**
     * 验证手机号码
     *
     * @param mobiles 要检查的号码
     * @return [0-9]{5,9}
     */
    public static boolean isMobileNO(String mobiles) {
        boolean flag = false;
        try {
            Pattern p = Pattern
                    // .compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
                    .compile("^(1)\\d{10}$");
            Matcher m = p.matcher(mobiles);
            flag = m.matches();
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }

    /**
     * 检查字符串是否是数字
     *
     * @param number 要检查的字符串
     */
    public static boolean isNum(String number) {
        boolean flag = false;
        try {
            Pattern p = Pattern.compile("^[0-9]{5}$");
            Matcher m = p.matcher(number);
            flag = m.matches();
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }

    /**
     * 判断某张表是否存在
     */
    public static boolean tabbleIsExist(SQLiteDatabase db, String tableName) {
        boolean result = false;
        if (tableName == null) {
            return false;
        }
        Cursor cursor = null;
        try {
            String sql = "select count(*) as c from Sqlite_master  where type ='table' and name ='"
                    + tableName.trim() + "' ";
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                int count = cursor.getInt(0);
                if (count > 0) {
                    result = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 判断某张表中是否存在某字段(注，该方法无法判断表是否存在，因此应与isTableExist一起使用)
     */
    public static boolean isColumnExist(SQLiteDatabase db, String tableName,
                                        String columnName) {
        boolean result = false;
        if (tableName == null) {
            return false;
        }
        try {
            Cursor cursor = null;
            String sql = "select count(1) as c from sqlite_master where type ='table' and name ='"
                    + tableName.trim()
                    + "' and sql like '%"
                    + columnName.trim() + "%'";
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                int count = cursor.getInt(0);
                if (count > 0) {
                    result = true;
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 解析json封装成对象
     *
     * @param object     实体类
     * @param jsonObejct JSONObject
     */
    public static Object ResolveJSON(Object object, JSONObject jsonObejct) {
        Class<?> demo = null;
        demo = object.getClass();
        Field[] fields = demo.getDeclaredFields();
        for (Field field : fields) {
            try {
                if (String.class == field.getType()) {
                    field.set(object, jsonObejct.optString(field.getName()));
                } else if (int.class == field.getType()) {
                    field.set(object, jsonObejct.optInt(field.getName()));
                } else if (Boolean.TYPE == field.getType()) {
                    field.set(object, jsonObejct.optBoolean(field.getName()));
                } else if (Double.TYPE == field.getType()) {
                    field.set(object, jsonObejct.optDouble(field.getName()));
                } else if (Long.TYPE == field.getType()) {
                    field.set(object, jsonObejct.optLong(field.getName()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return demo;
    }

    /**
     * 判断是否为平板
     */
    public static boolean isPad(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        // 屏幕尺寸
        double screenInches = Math.sqrt(x + y);
        return screenInches >= 7.0;
    }

    /**
     * 计算触摸的弧度
     *
     * @param x        原点X
     * @param y        原点Y
     * @param a        触点X
     * @param b        触点Y
     * @param Quadrant 象限
     * @return 弧度值
     */
    public static int getAngle(double x, double y, double a, double b,
                               int Quadrant) {
        double i = Math.abs(x - a);
        double j = Math.abs(y - b);
        double k = Math.sqrt(Math.pow(i, 2) + Math.pow(j, 2));
        int addAngle = (4 - Quadrant) * 90;
        switch (Quadrant) {
            case 1:
                return getAngle(j, i, k) + addAngle;
            case 2:
                return getAngle(i, j, k) + addAngle;
            case 3:
                return getAngle(j, i, k) + addAngle;
            case 4:
                return getAngle(i, j, k) + addAngle;
            default:
                return 0;
        }
    }

    /**
     * 计算弧度角
     *
     * @param a 轴坐标长
     * @param b 轴外长
     * @param c 斜边
     * @return
     */
    public static int getAngle(double a, double b, double c) {
        DecimalFormat df = new DecimalFormat("#0");
        // 计算弧度表示的角
        double B = Math.asin((a * a + c * c - b * b) / (2.0 * a * c));
        // 用角度表示的角
        B = Math.toDegrees(B);
        // 格式化数据，保留两位小数
        return Integer.valueOf(df.format(B));
    }

    /**
     * 判断wifi 是否可用
     *
     * @param context
     * @return
     * @throws Exception
     */
    public static boolean isWifiDataEnable(Context context) throws Exception {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isWifiDataEnable = false;
        isWifiDataEnable = connectivityManager.getNetworkInfo(
                ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
        return isWifiDataEnable;
    }

    /**
     * 检查输入是否非空
     *
     * @param context Context对象
     * @param view    需要检测的TextVie或其子类（EditText）
     * @return 是否为空
     */
    public static boolean detectInputBlank(Context context, View... view) {
        for (View aView : view) {
            TextView textView = (TextView) aView;
            if (!AppUtil.isNotBlank(textView.getText().toString())) {
                try {
                    AppUtil.showToastMsg(context, textView.getTag().toString()
                            + "不能为空");
                } catch (Exception e) {
                    AppUtil.showToastMsg(context, "部分参数错误");
                }
                textView.requestFocus();
                return false;
            }
        }
        return true;
    }

    /**
     * 水平方向模糊度
     */
    private static float hRadius = 5;
    /**
     * 竖直方向模糊度
     */
    private static float vRadius = 5;
    /**
     * 模糊迭代度
     */
    private static int iterations = 2;

    /**
     * 高斯模糊
     */
    public static Drawable BoxBlurFilter(Bitmap bmp) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        int[] inPixels = new int[width * height];
        int[] outPixels = new int[width * height];
        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Config.ARGB_8888);
        bmp.getPixels(inPixels, 0, width, 0, 0, width, height);
        for (int i = 0; i < iterations; i++) {
            blur(inPixels, outPixels, width, height, hRadius);
            blur(outPixels, inPixels, height, width, vRadius);
        }
        blurFractional(inPixels, outPixels, width, height, hRadius);
        blurFractional(outPixels, inPixels, height, width, vRadius);
        bitmap.setPixels(inPixels, 0, width, 0, 0, width, height);
        return new BitmapDrawable(UIUtils.getContext().getResources(), bitmap);
    }

    /**
     * 模糊图片
     *
     * @param in     图片的数组
     * @param out    模糊后的图片
     * @param width  宽度
     * @param height 高度
     * @param radius 半径
     */
    public static void blur(int[] in, int[] out, int width, int height,
                            float radius) {
        int widthMinus1 = width - 1;
        int r = (int) radius;
        int tableSize = 2 * r + 1;
        int divide[] = new int[256 * tableSize];

        for (int i = 0; i < 256 * tableSize; i++)
            divide[i] = i / tableSize;

        int inIndex = 0;

        for (int y = 0; y < height; y++) {
            int outIndex = y;
            int ta = 0, tr = 0, tg = 0, tb = 0;

            for (int i = -r; i <= r; i++) {
                int rgb = in[inIndex + clamp(i, 0, width - 1)];
                ta += (rgb >> 24) & 0xff;
                tr += (rgb >> 16) & 0xff;
                tg += (rgb >> 8) & 0xff;
                tb += rgb & 0xff;
            }

            for (int x = 0; x < width; x++) {
                out[outIndex] = (divide[ta] << 24) | (divide[tr] << 16)
                        | (divide[tg] << 8) | divide[tb];

                int i1 = x + r + 1;
                if (i1 > widthMinus1)
                    i1 = widthMinus1;
                int i2 = x - r;
                if (i2 < 0)
                    i2 = 0;
                int rgb1 = in[inIndex + i1];
                int rgb2 = in[inIndex + i2];

                ta += ((rgb1 >> 24) & 0xff) - ((rgb2 >> 24) & 0xff);
                tr += ((rgb1 & 0xff0000) - (rgb2 & 0xff0000)) >> 16;
                tg += ((rgb1 & 0xff00) - (rgb2 & 0xff00)) >> 8;
                tb += (rgb1 & 0xff) - (rgb2 & 0xff);
                outIndex += height;
            }
            inIndex += width;
        }
    }

    /**
     * 高斯模糊
     *
     * @param in     图片的数组
     * @param out    模糊后的图片
     * @param width  宽度
     * @param height 高度
     * @param radius 半径
     */
    public static void blurFractional(int[] in, int[] out, int width,
                                      int height, float radius) {
        radius -= (int) radius;
        float f = 1.0f / (1 + 2 * radius);
        int inIndex = 0;

        for (int y = 0; y < height; y++) {
            int outIndex = y;

            out[outIndex] = in[0];
            outIndex += height;
            for (int x = 1; x < width - 1; x++) {
                int i = inIndex + x;
                int rgb1 = in[i - 1];
                int rgb2 = in[i];
                int rgb3 = in[i + 1];

                int a1 = (rgb1 >> 24) & 0xff;
                int r1 = (rgb1 >> 16) & 0xff;
                int g1 = (rgb1 >> 8) & 0xff;
                int b1 = rgb1 & 0xff;
                int a2 = (rgb2 >> 24) & 0xff;
                int r2 = (rgb2 >> 16) & 0xff;
                int g2 = (rgb2 >> 8) & 0xff;
                int b2 = rgb2 & 0xff;
                int a3 = (rgb3 >> 24) & 0xff;
                int r3 = (rgb3 >> 16) & 0xff;
                int g3 = (rgb3 >> 8) & 0xff;
                int b3 = rgb3 & 0xff;
                a1 = a2 + (int) ((a1 + a3) * radius);
                r1 = r2 + (int) ((r1 + r3) * radius);
                g1 = g2 + (int) ((g1 + g3) * radius);
                b1 = b2 + (int) ((b1 + b3) * radius);
                a1 *= f;
                r1 *= f;
                g1 *= f;
                b1 *= f;
                out[outIndex] = (a1 << 24) | (r1 << 16) | (g1 << 8) | b1;
                outIndex += height;
            }
            out[outIndex] = in[width - 1];
            inIndex += width;
        }
    }

    /**
     * 求三个数种的最大值
     *
     * @return 最大值
     */
    public static int clamp(int x, int a, int b) {
        return (x < a) ? a : (x > b) ? b : x;
    }

    /**
     * 图片转换为数组
     *
     * @param bmp         图片对象
     * @param needRecycle 转换完成后是否需要回收
     * @return 转换后的数组
     */
    public static byte[] bmpToByteArray(final Bitmap bmp,
                                        final boolean needRecycle) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(CompressFormat.PNG, 100, output);
        if (needRecycle) {
            bmp.recycle();
        }

        byte[] result = output.toByteArray();
        try {
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
    //如"index.jsp ? Action=del&id=123",解析出Action:del,id:123存入map中;

    /**
     * 解析出url参数中的键值对
     *
     * @param URL url地址
     * @return url请求参数部分
     */
    public static Map<String, String> URLRequest(String URL) {
        Map<String, String> mapRequest = new HashMap<String, String>();
        String[] arrSplit = null;
        String strUrlParam = TruncateUrlPage(URL);
        if (strUrlParam == null) {
            return mapRequest;
        }
        // 每个键值为一组
        arrSplit = strUrlParam.split("[&]");
        for (String strSplit : arrSplit) {
            String[] arrSplitEqual = null;
            arrSplitEqual = strSplit.split("[=]");
            // 解析出键值
            if (arrSplitEqual.length > 1) {
                // 正确解析
                mapRequest.put(arrSplitEqual[0], arrSplitEqual[1]);
            } else {
                if (arrSplitEqual[0] != "") {
                    // 只有参数没有值，不加入
                    mapRequest.put(arrSplitEqual[0], "");
                }
            }
        }
        return mapRequest;
    }

    /**
     * 去掉url中的路径，留下请求参数部分
     *
     * @param strURL url地址
     * @return url请求参数部分
     */
    private static String TruncateUrlPage(String strURL) {
        String strAllParam = null;
        String[] arrSplit = null;
        strURL = strURL.trim().toLowerCase();
        arrSplit = strURL.split("[?]");
        if (strURL.length() > 1) {
            if (arrSplit.length > 1) {
                if (arrSplit[1] != null) {
                    strAllParam = arrSplit[1];
                }
            }
        }
        return strAllParam;
    }

    /**
     * 是否安装微信
     */
    public static boolean isInstallWx(Activity mActivity) {
        try {

            PackageManager manager = mActivity.getPackageManager();

            PackageInfo info = manager.getPackageInfo("com.tencent.mm",
                    PackageManager.GET_ACTIVITIES);

            if (info != null) {

                return true;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return false;
    }

    /**
     * 输入流转字符串
     *
     * @param is 输入流
     * @return 返回的字符串
     */
    public static String inputStream2String(InputStream is) {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        StringBuffer buffer = new StringBuffer();
        String line = "";
        try {
            while ((line = in.readLine()) != null) {
                buffer.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }

    /**
     * 删除所有文件
     *
     * @param path 文件地址
     * @return 是否删除成功
     */
    public static boolean delAllFile(String path) {
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return false;
        }
        if (!file.isDirectory()) {
            return false;
        }
        String[] tempList = file.list();
        File temp = null;
        for (String aTempList : tempList) {
            if (path.endsWith(File.separator)) {
                temp = new File(path + aTempList);
            } else {
                temp = new File(path + File.separator + aTempList);
            }
            if (temp.isFile()) {
                temp.delete();
            }
            if (temp.isDirectory()) {
                delAllFile(path + "/" + aTempList);// 先删除文件夹里面的文件
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 隐藏状态栏实现全屏
     *
     * @param window window对象
     */
    public static void enterLightsOutMode(Window window) {
        WindowManager.LayoutParams params = window.getAttributes();
        params.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE;
        window.setAttributes(params);
    }

//    /**
//     * 获取设备信息
//     *
//     * @param context Context 对象
//     * @return 设备信息
//     */
//    public static String getDeviceInfo(Context context) {
//        try {
//            org.json.JSONObject json = new org.json.JSONObject();
//            android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
//                    .getSystemService(Context.TELEPHONY_SERVICE);
//
//            String device_id = tm.getDeviceId();
//
//            android.net.wifi.WifiManager wifi = (android.net.wifi.WifiManager) context
//                    .getSystemService(Context.WIFI_SERVICE);
//
//            String mac = wifi.getConnectionInfo().getMacAddress();
//            json.put("mac", mac);
//
//            if (StringUtils.isEmpty(device_id)) {
//                device_id = mac;
//            }
//
//            if (StringUtils.isEmpty(device_id)) {
//                device_id = android.provider.Settings.Secure.getString(
//                        context.getContentResolver(),
//                        android.provider.Settings.Secure.ANDROID_ID);
//            }
//
//            json.put("device_id", device_id);
//
//            return json.toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

//    /**
//     * 获取设备唯一标识
//     *
//     * @param context Context对象
//     * @return 标识字符串
//     */
//    public static String getUniqueIdentification(Context context) {
//        String str = "www." + getDeviceModel(context) + getIMEI(context)
//                + getLocalMacAddress(context) + "vcinema.cn";
//        str = MD5.getMessageDigest(str.getBytes());
//        return str;
//    }


    /**
     * 获取屏幕宽度
     *
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    /**
     * 获取屏幕高度
     *
     * @param context
     * @return
     */
    public static int getScreenHeight(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        dm = context.getResources().getDisplayMetrics();
        return dm.heightPixels;
    }

    /**
     * 判断某个界面是否在前台
     *
     * @param context
     * @param className 某个界面名称
     */
    private boolean isForeground(Context context, String className) {
        if (context == null || StringUtils.isEmpty(className)) {
            return false;
        }
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(1);
        if (list != null && list.size() > 0) {
            ComponentName cpn = list.get(0).topActivity;
            if (className.equals(cpn.getClassName())) {
                return true;
            }
        }

        return false;
    }

    /*
     * Function  :   封装请求体信息
     * Param     :   params请求体内容，encode编码格式
     * Author    :   博客园-依旧淡然
     */
    public static StringBuffer getRequestData(Map<String, String> params, String encode) {
        StringBuffer stringBuffer = new StringBuffer();        //存储封装好的请求体信息
        try {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                stringBuffer.append(entry.getKey())
                        .append("=")
                        .append(URLEncoder.encode(entry.getValue(), encode))
                        .append("&");
            }
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);    //删除最后的一个"&"
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuffer;
    }

    /**
     * 得到屏幕宽度
     *
     * @return 宽度
     */
    public static int getScreenWidth(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    /**
     * 得到屏幕高度
     *
     * @return 高度
     */
    public static int getScreenHeight(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    /**
     * 制作GET请求链接 <br>
     * 通过参数拼接GET请求链接
     *
     * @param url            请求地址
     * @param nameValuePairs 请求参数
     * @return String
     */
    public static String makeGetUrl(String url, NameValuePair... nameValuePairs) {
//        LogUtils.i("请求地址="+url);
//        for (NameValuePair name:nameValuePairs){
//            LogUtils.i("请求参数1="+name.getName());
//            LogUtils.i("请求参数2="+name.getValue());
//        }
        StringBuilder sb = new StringBuilder();
        sb.append(url);
        if (nameValuePairs.length > 0) {
            for (int i = 0; i < nameValuePairs.length; i++) {
                if (i == 0) {
                    sb.append("?").append(nameValuePairs[i].getName())
                            .append("=");
                }
                if (i > 0) {
                    sb.append("&").append(nameValuePairs[i].getName())
                            .append("=");
                }
                sb.append(nameValuePairs[i].getValue());
            }
        }
        return sb.toString().replaceAll(" ", "");
    }

    /**
     * 检测是否有emoji表情
     */
    public static boolean containsEmoji(String source) {
        int len = source.length();
        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);
            if (!isEmojiCharacter(codePoint)) { //如果不能匹配,则该字符是Emoji表情
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否是Emoji
     *
     * @param codePoint 比较的单个字符
     */
    private static boolean isEmojiCharacter(char codePoint) {
        return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) || (codePoint == 0xD) || ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || ((codePoint >= 0x10000));
    }


}
