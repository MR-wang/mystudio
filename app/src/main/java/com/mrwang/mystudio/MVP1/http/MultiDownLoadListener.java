package com.mrwang.mystudio.MVP1.http;

/**
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-07-13
 * Time: 15:27
 */
public interface MultiDownLoadListener {
    /**
     * 线程开始时
     */
    public void onStart();

    /**
     * 线程进行中
     * @param total 文件总大小
     * @param current 当前进度
     */
    public void onLoading(long total, long current);

    /**
     * 完成下载后
     * @param filePath 下载的文件路径
     */
    public void onSuccess(String filePath);
}  
