package com.mrwang.mystudio.MVP1.utils;

import android.content.Context;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.mrwang.mystudio.R;


/**
 * Toast 工具类
 *
 * @author chenbc
 * @Description 设置指定的时间隐藏, 可以衍生为弹窗辅助通用类
 * @date 2015-11-11
 */
public class ToastUtil {
    private static LongToast toast;
    private static TextView textView;

    /**
     * 游戏toast
     *
     * @param context 上下文
     * @param content duration 6*1000
     */
    public static void po(Context context, String content) {
        po(context, content, 6 * 1000);
    }
    public static void poCenter(Context context, String content) {
        poCenter(context, content, 6 * 1000);
    }

    /**
     * 游戏toast
     *
     * @param context 上下文
     * @param content 资源
     */
    public static void po(Context context, int content) {
        po(context, context.getString(content));
    }

    public static void po(Context context, String content, int duration) {
        if (toast == null) {
            toast = new LongToast(context);
            View layout = UIUtils.inflate(R.layout.item_toast_po);
            textView = (TextView) layout.findViewById(R.id.content_text);
            toast.setDuration(duration);
            int top = context.getResources().getDimensionPixelOffset(R.dimen.dp_15);
            toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, top);
            toast.setView(layout);
        }
        textView.setText(content);
        toast.show();
    }

    public static void poCenter(Context context, String content, int duration) {
        if (toast == null) {
            toast = new LongToast(context);
            View layout = UIUtils.inflate(R.layout.item_toast_po);
            textView = (TextView) layout.findViewById(R.id.content_text);
            toast.setDuration(duration);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.setView(layout);
        }
        textView.setText(content);
        toast.show();
    }


    public static void cancel(){
        if (toast!=null){
            toast.removeView();
        }
    }

    public static class LongToast {
        public static final int LENGTH_LONG = 3500;
        public static final int LENGTH_SHORT = 500;
        private WindowManager mWindowManager;
        private WindowManager.LayoutParams mWindowParams;
        private View toastView;
        private Context mContext;
        private Handler mHandler;
        private String mToastContent = "";
        private int duration = 0;
        private int animStyleId = android.R.style.Animation_Toast;
        private int mGravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;

        private final Runnable timerRunnable = new Runnable() {
            @Override
            public void run() {
                removeView();
            }
        };

        public LongToast(Context context) {
            Context ctx = context.getApplicationContext();
            if (ctx == null) {
                ctx = context;
            }
            this.mContext = ctx;
            mWindowManager = (WindowManager) mContext
                    .getSystemService(Context.WINDOW_SERVICE);
            init();
        }

        private void init() {
            mWindowParams = new WindowManager.LayoutParams();
            mWindowParams.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
            mWindowParams.alpha = 1.0f;
            mWindowParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
            mWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            mWindowParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
            mWindowParams.format = PixelFormat.TRANSLUCENT;
            mWindowParams.type = WindowManager.LayoutParams.TYPE_TOAST;
            mWindowParams.packageName = mContext.getPackageName();
            mWindowParams.windowAnimations = animStyleId;
            mWindowParams.y = mContext.getResources().getDisplayMetrics().widthPixels / 5;
        }

        private View getDefaultToastView() {
            TextView view = new TextView(mContext);
            view.setText(mToastContent);
            view.setGravity(Gravity.CENTER_VERTICAL | Gravity.START);
            view.setFocusable(false);
            view.setClickable(false);
            view.setFocusableInTouchMode(false);
            view.setTextColor(android.graphics.Color.WHITE);
            Drawable drawable = mContext.getResources().getDrawable(android.R.drawable.toast_frame);
            if (Build.VERSION.SDK_INT < 16) {
                view.setBackgroundDrawable(drawable);
            } else {
                view.setBackground(drawable);
            }
            return view;
        }

        public void show() {
            removeView();
            if (toastView == null) {
                toastView = getDefaultToastView();
            }
            mWindowParams.gravity = GravityCompat.getAbsoluteGravity(mGravity, ViewCompat.getLayoutDirection(toastView));
            mWindowManager.addView(toastView, mWindowParams);
            if (mHandler == null) {
                mHandler = new Handler();
            }
            mHandler.postDelayed(timerRunnable, duration);
        }

        public void removeView() {
            if (toastView != null && toastView.getParent() != null) {
                mWindowManager.removeView(toastView);
                mHandler.removeCallbacks(timerRunnable);
            }
        }

        public void setGravity(int gravity, int xOffset, int yOffset) {
            mGravity = gravity;
            mWindowParams.x = xOffset;
            mWindowParams.y = yOffset;
        }

        public LongToast makeText(Context context, String content, int duration) {
            LongToast helper = new LongToast(context);
            helper.setDuration(duration);
            helper.setContent(content);
            return helper;
        }


        public LongToast makeText(Context context, int strId, int duration) {
            LongToast helper = new LongToast(context);
            helper.setDuration(duration);
            helper.setContent(context.getString(strId));
            return helper;
        }

        public LongToast setContent(String content) {
            this.mToastContent = content;
            return this;
        }

        public LongToast setDuration(int duration) {
            this.duration = duration;
            return this;
        }

        public LongToast setAnimation(int animStyleId) {
            this.animStyleId = animStyleId;
            mWindowParams.windowAnimations = this.animStyleId;
            return this;
        }

        public LongToast setView(View view) {
            this.toastView = view;
            return this;
        }
    }


}

