package com.mrwang.mystudio.MVP1.module.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.mrwang.mystudio.R;

/**
 * 承载Fragment的Activity基类
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-09-05
 * Time: 00:16
 */
public class FragActivity extends FragmentActivity {
    private int framLayoutId = R.id.layout;
    //private FrameLayout mMainLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_main);
        HomeFragment homeFragment=new HomeFragment();
        setFragment(homeFragment);
        Log.i("TAG", "承载Fragment的Activity基类");

    }

//    @Override
//    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
//        super.onCreate(savedInstanceState, persistentState);
//
//
//        //HomeFragment homeFragment=new HomeFragment();
//        //getSupportFragmentManager().beginTransaction().replace(R.id.layout, homeFragment).commit();
////        mMainLayout = new FrameLayout(this);
////        mMainLayout.setId(1);
////        setContentView(mMainLayout);
////        HomeFragment homeFragment=new HomeFragment();
////        setFragment(homeFragment);
//    }

    /**
     * 设置最底层的页面
     */
    private void setFragment(Fragment mTargetFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(framLayoutId, mTargetFragment, mTargetFragment.getClass().getName())
                .commit();
    }

    /**
     * 弹出页面
     */
    public void popFragmant(Fragment from, Fragment to) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.hide(from)
                .setCustomAnimations(R.anim.next_in, R.anim.next_out)//必须在add、remove、replace调用之前被设置，否则不起作用。
                .add(framLayoutId, to, to.getClass().getName())
                .addToBackStack(to.getClass().getName())
                .commit();
    }

    /**
     * 弹出页面， dialog 形式
     */
    public void popFragmant(Fragment to) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (!to.isAdded()) {
            transaction
                    .setCustomAnimations(R.anim.dialog_in, R.anim.dialog_out)
                    .add(framLayoutId, to, to.getClass().getName())
                    .addToBackStack(to.getClass().getName())
                    .commit();
        }
    }

    /**
     * 关闭页面
     */
    public void closeFragment(Fragment mTargetFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(mTargetFragment).commit();
        getSupportFragmentManager().popBackStack();
    }

    /**
     * 关闭最顶部的fragment
     */
    public void close(){
        getSupportFragmentManager().popBackStack();
    }

    /**
     * 关闭所有页面
     */
    public void closeAllFragment() {
        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < backStackCount; i++) {
            int backStackId = getSupportFragmentManager().getBackStackEntryAt(i).getId();
            getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

//    // 最底层的页面
//    public void onEvent(Fragment fragment) {
//        setFragment(fragment);
//    }
//
//    // 弹出页面
//    public void onEvent(Event.OpenFragmentEvent event) {
//        popFragmant(event.fromFragment, event.toFragment);
//    }
//
//    // 关闭页面
//    public void onEvent(Event.CloseFragmentEvent event) {
//        closeFragment(event.mFragment);
//    }
//
//    // 关闭所有页面
//    public void onEvent(Event.CloswAllFragmentEvent event) {
//        closeAllFragment();
//    }
//
//    // 弹出页面， dialog 形式
//    public void onEvent(Event.PopFragment event) {
//        popFragmant(event.toFragment);
//    }
}  
