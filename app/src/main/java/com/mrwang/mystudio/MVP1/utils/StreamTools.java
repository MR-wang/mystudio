package com.mrwang.mystudio.MVP1.utils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class StreamTools {
	public static String readStream(InputStream is) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len = -1;
			while ((len = is.read(buffer)) != -1) {
				baos.write(buffer, 0, len);
			}
			is.close();
			return new String(baos.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
