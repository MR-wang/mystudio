package com.mrwang.mystudio.MVP1.http;


import com.mrwang.mystudio.MVP1.utils.LogUtils;
import com.mrwang.mystudio.MVP1.utils.UIUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 网络请求的工具类
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-06-01
 * Time: 16:45
 */
public enum NetUtils {
    INSTANCE;

    private final LruMemCache cache;
    private boolean isFromCache;

    NetUtils() {
        cache = new LruMemCache();
    }

    private boolean isCache=true;
    private int cacheTime=5000;

    /**
     * 发送json到服务器  可以是Post Get 或者put
     *
     * @param path      url地址
     * @param json      jsonObject对象
     * @param linstener 回调
     */
    public void sendJson(Method method, final String path, final JSONObject json, final Linstener linstener) {
        sendMore(method, path, linstener, new ConnListener() {
            @Override
            public void setConn(HttpURLConnection conn) {
                conn.setRequestProperty("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)");
                //区别3： 必须指定两个请求的参数。
                conn.setRequestProperty("Content-Type", "application/json");//请求的类型 表单数据
                //区别4： 记得设置把数据写给服务器。
                conn.setDoOutput(true);//设置向服务器写数据。
                conn.setDoInput(true);
                conn.setUseCaches(false);
                try {
                    conn.getOutputStream().write(String.valueOf(json).getBytes());//把数据以流的方式写给服务器。
                } catch (IOException e) {
                    e.printStackTrace();
                    LogUtils.e("向服务器写入json失败");
                }
            }
        });
    }

    /**
     * 向服务器发送请求
     *
     * @param method    请求方式 枚举类型 Get或者post
     * @param path      url地址
     * @param linstener 回调
     */
    public void send(final Method method, final String path, final Linstener linstener) {
        sendMore(method, path, linstener, null);
    }

    /**
     * 向服务器发送请求 可以定制更多的连接类型
     *
     * @param method       请求方式 枚举类型 Get或者post
     * @param path         url地址
     * @param linstener    回调
     * @param connListener 设置连接的回调
     */
    public void sendMore(final Method method, final String path, final Linstener linstener, final ConnListener connListener) {
        ThreadManager.getLongPool().execute(new Runnable() {
            @Override
            public void run() {
                processNet(method, path, connListener, linstener, true);
            }
        });
    }

    /**
     * 从网络获取数据 不使用线程池
     * @param method 请求方式
     * @param path 请求地址
     * @param linstener
     * @return
     */
    public Response getNetword(Method method, String path, Linstener linstener){
        return processNet(method, path, null, linstener, false);
    }

    private Response processNet(Method method, String path, ConnListener connListener, Linstener linstener, boolean isPost) {
        Response response=null;
        if (isCache){
            response = cache.get(path);
            if (response!=null&& (System.currentTimeMillis()-response.currentTime<cacheTime)){
                isFromCache = true;
                //LogUtils.i("走缓存");
                post(response.isPost,path, response.isSucess, response.code, response.result, response.linstener);
                return response;
            }
        }
        //LogUtils.i("从网络请求");
        isFromCache=false;

        try {
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(method.getMethod());// 大写
            conn.setReadTimeout(5000);
            conn.setConnectTimeout(8000);
            if (connListener != null) {
                connListener.setConn(conn);
            }
            int code = conn.getResponseCode();
            InputStream is;
            if (code >= 200 && code < 300) {
                is = conn.getInputStream();
                response = post(isPost, path, true, code, UIUtils.readStream(is), linstener);
            } else {
                is = conn.getErrorStream();
                response=post(isPost,path, false, code, UIUtils.readStream(is), linstener);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            response=post(isPost,path, false, 10086, e.getMessage() + " url地址错误", linstener);

        } catch (IOException e) {
            e.printStackTrace();
            response=post(isPost, path,false, 10000, e.getMessage() + " IO异常,请检查网络", linstener);
        }
        return response;
    }



    public Response post(boolean isPost, String path, boolean isSucess, final int code, final String result, final Linstener linstener) {
        Response response=new Response(isPost,path,isSucess,code,result,linstener, System.currentTimeMillis());
        if (isCache&&!isFromCache){
            cache.put(path,response);
        }
        if (isPost) {
            postRunnable(isSucess, code, result, linstener);
        } else {
            callBack(linstener, isSucess, code, result);
        }
        return response;
    }


    public enum Method {
        POST("POST"),
        GET("GET"),
        PUT("PUT");
        String method;

        Method(String get) {
            this.method = get;
        }

        public String getMethod() {
            return method;
        }
    }

    public interface ConnListener {
        void setConn(HttpURLConnection conn);
    }

    public interface Linstener {
        /**
         * 请求成功时的回调
         *  @param requestCode 服务器返回码
         * @param result      服务器返回的字符串
         */
        void onSucessInfo(int requestCode, String result);

        /**
         * 请求失败时的回调
         *
         * @param requestCode 服务器返回码
         * @param errorMsg    服务器返回的字符串
         */
        void onFailure(int requestCode, String errorMsg);
    }

    /**
     * 将结果发送到主线程中执行
     *
     * @param isSucess  是否请求成功
     * @param code      服务器返回码
     * @param result    服务器输出流
     * @param linstener 回调
     */
    private void postRunnable(final boolean isSucess, final int code, final String result, final Linstener linstener) {
        UIUtils.post(new Runnable() {
            @Override
            public void run() {
                callBack(linstener, isSucess, code, result);
            }
        });
    }

    private void callBack(Linstener linstener, boolean isSucess, int code, String result) {
        if (linstener != null) {
            if (isSucess) {
                linstener.onSucessInfo(code, result);
            } else {
                linstener.onFailure(code, result);
            }

        }
    }

    /**
     * 设置是否需要缓存 默认开启缓存
     * @param isCache 是否需要缓存
     */
    public void setCache(boolean isCache){
        this.isCache=isCache;
    }

    /**
     * 设置缓存时间 默认缓存时间5s
     * @param cacheTime 缓存时间
     */
    public void setCacheTime(int cacheTime){
        this.cacheTime=cacheTime;
    }
}  
