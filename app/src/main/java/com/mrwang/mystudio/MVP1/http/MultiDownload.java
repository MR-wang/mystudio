package com.mrwang.mystudio.MVP1.http;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.SparseArray;

import com.mrwang.mystudio.MVP1.utils.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 多线程下载的实例代码 支持断点下载。
 *
 * @author Administrator
 */
public class MultiDownload {
    private MultiDownLoadListener listener;
    private int length;

    private MultiDownload() {
    }

    private String path = "http://192.168.16.140:8080/123.apk";

    private String fileName ;
    /**
     * 服务器资源等份成3份 每一份一个线程去下载
     */
    private int threadCount = 3;
    private int runningthreadcount;
    private SparseArray<Integer> currentsum = new SparseArray<>();


    private String filePath = FileUtils.getDownloadDir();

    public static class Builder {
        public MultiDownload download;

        private MultiDownload getDownload() {
            if (download == null) {
                download = new MultiDownload();
            }
            return download;
        }

        public Builder(String path) {
            getDownload().path = path;
        }

        /**
         * 设置文件路径
         *
         * @param filePath 文件路径
         */
        public Builder setfilePath(String filePath) {
            getDownload().filePath = filePath;
            return this;
        }

        /**
         * 设置下载线程数量
         *
         * @param threadCount 线程数量
         */
        public Builder setthreadCount(int threadCount) {
            getDownload().threadCount = threadCount;
            return this;
        }

        public Builder setListener(MultiDownLoadListener listener) {
            getDownload().listener = listener;
            return this;
        }

        /**
         * 开启多线程下载
         */
        public void start() {
            getDownload().downLoad();
        }
    }


    private void downLoad() {
        ThreadManager.getDownloadPool().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    load();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 从服务器获取下载信息 并开启线程
     *
     * @throws Exception
     */
    private void load() throws Exception {
        // 1.知道服务器资源都多大，等份成若干份。
        URL url = new URL(path);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setConnectTimeout(5000);
        fileName= getFileName(path);
        int code = conn.getResponseCode(); // 200 ok
        if (code == 200) {
            length = conn.getContentLength();
            LogUtils.i("服务器端文件资源的大小：" + length);
            int blocksize = length / threadCount;
            // 2.在客户端创建一个大小跟服务器一模一样的空文件。
            RandomAccessFile raf = new RandomAccessFile(filePath + "/" + fileName, "rw");
            raf.setLength(length);

            // 3.开启若干个不同的子线程下载对应的数据。
            runningthreadcount = threadCount;

            if (listener != null) {
                listener.onStart();
            }

            for (int id = 0; id < threadCount; id++) {
                int startindex = id * blocksize;
                int endindex = (id + 1) * blocksize - 1;
                if (id == (threadCount - 1)) {
                    endindex = length - 1;
                }
                LogUtils.i("线程id：" + id + "下载位置：" + startindex + "~" + endindex);
                ThreadManager.getDownloadPool().execute(new DownloadThread(id, startindex, endindex));
            }
        }
    }


    private class DownloadThread implements Runnable {
        /**
         * 线程id
         */
        private int threadid;
        /**
         * 开始位置
         */
        private int startindex;
        /**
         * 结束位置
         */
        private int endindex;

        public DownloadThread(int threadid, int startindex, int endindex) {
            this.threadid = threadid;
            this.startindex = startindex;
            this.endindex = endindex;
        }

        @Override
        public void run() {
            try {
                URL url = new URL(path);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setConnectTimeout(5000);

                //判断是否有记录的下载信息。如果有就从上次下载的位置继续下载。
                File file = new File(getSDPath() + threadid + ".txt");
                if (file.exists()) {
                    FileInputStream fis = new FileInputStream(file);
                    BufferedReader br = new BufferedReader(new InputStreamReader(fis));
                    String startline = br.readLine();
                    if (startline != null) {
                        startindex = Integer.parseInt(startline);
                    }
                    br.close();
                }

                conn.setRequestProperty("Range", "bytes=" + startindex + "-" + endindex);
                LogUtils.i("真实线程id：" + threadid + "下载位置：" + startindex + "~"
                        + endindex);
                int code = conn.getResponseCode(); //200 OK  206 OK(请求一部分数据成功）
                if (code == 206) {
                    InputStream is = conn.getInputStream();//服务器返回的输入流。
                    RandomAccessFile raf = new RandomAccessFile(filePath + "/" + fileName, "rw");
                    raf.seek(startindex);
                    int len;
                    byte[] buffer = new byte[1024 * 1024 * 8];
                    int total = 0;
                    while ((len = is.read(buffer)) != -1) {
                        raf.write(buffer, 0, len);
                        total += len;//得到的是 当前下载的数据的总长度。需要把当前线程下载的位置信息存储起来。
                        //存储当前线程已经下载到的位置。
                        int currentposition = startindex + total;

                        LogUtils.i("正在下载...."+threadid+"="+total +"="+length);

                        if (listener != null) {
                            synchronized (MultiDownload.class) {
                                setProcess(total);
                            }
                        }

                        RandomAccessFile af = new RandomAccessFile(getSDPath() + threadid + ".txt", "rwd");//每次更新文件都同步到底层的存储设备
                        af.write(String.valueOf(currentposition).getBytes());
                        af.close();
                    }
                    raf.close();
                    is.close();
                    LogUtils.i("线程：" + threadid + "下载完毕了");
                    //什么时候所有的线程都执行完了？
                    synchronized (MultiDownload.class) {
                        runningthreadcount--;
                        if (runningthreadcount <= 0) {
                            LogUtils.i("所有线程都执行完毕了.");
                            for (int i = 0; i < threadCount; i++) {
                                File f = new File(getSDPath() + i + ".txt");

                                boolean delete = f.delete();
                                LogUtils.i("文件删除" + delete);
                            }
                            if (listener != null) {
                                listener.onSuccess(filePath + "/" + fileName);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void setProcess(int total) {
            currentsum.put(threadid, total);
            int sum = 0;
            for (int i = 0; i < currentsum.size(); i++) {
                if (currentsum.get(i)==null)
                    continue;
                sum += currentsum.get(i);
            }
            listener.onLoading(length, sum);
        }

        @NonNull
        private String getSDPath() {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
        }
    }

    /**
     * 获取文件名
     */
    private String getFileName(String path) {
        int start = path.lastIndexOf("/") + 1;
        return path.substring(start);
    }

}
