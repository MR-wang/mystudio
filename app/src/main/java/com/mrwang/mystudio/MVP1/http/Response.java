package com.mrwang.mystudio.MVP1.http;

/**
 * 请求结果缓存类
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-07-15
 * Time: 12:10
 */
public class Response {
    public boolean isPost;
    public String result;
    public String url;
    public int code;
    public boolean isSucess;
    public NetUtils.Linstener linstener;
    public long currentTime;

    Response(boolean isPost, String url, boolean isSucess, int code, String result, NetUtils.Linstener linstener, long currentTime) {
        this.isPost = isPost;
        this.result = result;
        this.url = url;
        this.code = code;
        this.isSucess = isSucess;
        this.linstener = linstener;
        this.currentTime = currentTime;
    }


}
