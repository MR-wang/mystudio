package com.mrwang.mystudio.MVP1.model.bean;

/**
 * DataBinding高级用法
 * User: chengwangyong(chengwangyong@vcinema.com)
 * Date: 2015-06-29
 * Time: 13:24
 */
public class MyStringUtils {

    public static String capitalize(final String word) {
        if (word.length() > 1) {
            return String.valueOf(word.charAt(0)).toUpperCase() + word.substring(1);
        }
        return word;
    }
}  
